## 引言：
作为一个有社交网站情节的老站长，对于地方社区运营有着极高的热情，在地方社交平台运营进行着长期的探索；未来以用户交流分享为中心的站点运营，包括地方社区、知识社区的运营，将以圈子私域的形态长期存在，并且将会越来越受到用户的欢迎和认可，通过建立互动、个性化的平台，您可以为用户提供更好的体验和服务，让他们更好地分享自己的经验和知识。。

## 邻客互动(LnkSns)
简洁、实用、低成本的圈子系统，开箱即用，极大地降低了产品的研发和技术成本，能够快速投入产品运营工作。

同时主打简约精美UI，助力企业实现私域运营，最终实现快速积累客户、会员数据分析、智能转化客户、有效提高销售、提升会员运营的效果。

后台基于 ThinkPhp6.0+LnkAdmin，UI采用Element Plus框架，系统高效稳定，UI美观精致；

我们深知做好一件事已经不容易，运营好站点更不容易，我们欢迎更多有社区站点运营的朋友，加入我们的讨论交流群，一同探索交流，共同打造一款适合社区圈子运营的系统。

## 安装、交流、讨论、BUG修复请加群，微信 nzkd01
1、安装文档见群公告

2、如果你只专注于产品运营，对程序的安装不感兴趣，可以联系我们，由我们免费帮助你安装、部署。

## 演示
后台：https://demo.lnksns.vip/super/index.html  

帐号密码：admin
## 小程序
![小程序](https://gitee.com/snq3344/lnksns/raw/master/md/2wm.jpg)

## 邻客互动(LnkSns)系统亮点
UI精美，流畅体验

精致美观的UI页面，少即是多，用户可轻松上手操作，带来简单、舒适的用户体验，精心打磨，只为给你带来不一样的产品。

LnkSns后台基于TP6、Element Plus框架开发，逻辑清晰、布局合理！
后台UI框架为VUE3+ Element Plus框架
后台标准接口、前后端分离，页面加载跳转流畅，组件化开发，可复用，二次开发更方便。

前端框架为uni-app，后台前后端分离，可适配多端应用。

## 功能列表
1、动态支持文字、图集、视频；

2、回复支持emoji表情，支持二级回复；

3、支持快速获取微信头像、昵称；

4、支持调用微信手机号验证；

5、支持每日签到积分；

6、支持用户创建圈子；

7、支持私密圈子；

8、支持积分建圈；

9、支持积分入圈；

10、更多功能请下载发现。。。。

## 持续开发计划
1、积分激励插件（在线时长、发贴、回贴、任务奖励积分）；

2、社区活跃机器人插件（主动顶贴、回贴、收藏、点赞、关注好友）；

3、附件下载插件（积分下载、微信支付下载）；

4、广告插件（列表、动态、视频激励等）；

5、活动报名插件（免费、付费报名，积分报名、微信支付报名）；

6、积分商城插件；

7、投票功能插件；

8、采集发布插件；

9、同城分类插件（二手、招聘、房产）；

10、H5、PC、APP端；

11、适配抖音、百度小程序。

![输入图片说明](https://gitee.com/snq3344/lnksns/raw/master/md/T1.png)
![输入图片说明](https://gitee.com/snq3344/lnksns/raw/master/md/T2.png)

## 运行环境要求

> PHP7.2+，兼容PHP8.1

> Nignx 1.18~1.2.1/apache 2.2

> MySQL 5.7+

## 后端安装
~~~
composer install
~~~

## 小程序端
> 进入lnk-applet目录
> 使用HBuilder X 构建、发布

## 版权信息
🔐 使用lnksns无需付费。
本项目包含的第三方源码和二进制文件之版权信息另行标注。

## 特别鸣谢
💕 感谢巨人提供肩膀，排名不分先后
- [Thinkphp](http://www.thinkphp.cn/)
- [FastAdmin](https://gitee.com/karson/fastadmin)
- [Vue](https://github.com/vuejs/core)
- [vue-next-admin](https://gitee.com/lyt-top/vue-next-admin)
- [Element Plus](https://github.com/element-plus/element-plus)
- [TypeScript](https://github.com/microsoft/TypeScript)
- [vue-router](https://github.com/vuejs/vue-router-next)
- [vite](https://github.com/vitejs/vite)
- [LnkAdmin](https://gitee.com/DengJe/LiteAdmin)
- [Pinia](https://github.com/vuejs/pinia)
- [qinghang-qz](https://ext.dcloud.net.cn/plugin?id=15190)
- [Axios](https://github.com/axios/axios)
- [nprogress](https://github.com/rstacruz/nprogress)
- [screenfull](https://github.com/sindresorhus/screenfull.js)
- [mitt](https://github.com/developit/mitt)
- [sass](https://github.com/sass/sass)
- [wangEditor](https://github.com/wangeditor-team/wangEditor)
- [echarts](https://github.com/apache/echarts)
- [vueuse](https://github.com/vueuse/vueuse)