<?php
// +----------------------------------------------------------------------
// | 控制台配置
// +----------------------------------------------------------------------

use app\admin\command\LiteCommand;

return [
    // 指令定义
    'commands' => [
        'liteadmin'=>LiteCommand::class,
        'taskhook' => 'app\appcenter\command\TaskHook'
    ],
];
