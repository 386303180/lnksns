<?php

namespace app\lnksns\lib;


class SuPengJun
{
    public static function time_text($time)
    {
        if (!$time) return '未知';
        $time = strtotime($time);
        //将日期转化为时间戳
        $etime = time() - $time;
        switch ($etime) {
            case $etime <= 60:
                $result = '刚刚';
                break;
            case $etime > 60 && $etime <= 60 * 60:
                $result = floor($etime / 60) . '分钟前';
                break;
            case $etime > 60 * 60 && $etime <= 24 * 60 * 60:
                $result = date('Ymd', $time) == date('Ymd', time()) ? '今天 ' . date('H:i', $time) : '昨天 ' . date('H:i', $time);
                break;
            case $etime > 24 * 60 * 60 && $etime <= 2 * 24 * 60 * 60:
                $result = date('Ymd', $time) + 1 == date('Ymd', time()) ? '昨天 ' . date('H:i', $time) : '前天 ' . date('H:i', $time);
                break;
            case $etime > 2 * 24 * 60 * 60 && $etime <= 12 * 30 * 24 * 60 * 60:
                $result = date('Y', $time) == date('Y', time()) ? date('m/d', $time) : date('Y/m/d', $time);
                break;
            default:
                $result = date('Y/m/d', $time);
        }
        return $result;
    }

    public
    static function ip_text($model, $data)
    {
        $model['lat'] = $data['location']['lat'];
        $model['lng'] = $data['location']['lng'];
        $model['country'] = $data['ad_info']['nation'];
        $model['province'] = $data['ad_info']['province'];
        $model['city'] = $data['ad_info']['city'];
        $model['district'] = $data['ad_info']['district'];
        return $model;
    }

    public
    static function adds_text($text)
    {
        if (!$text) return 'IP未知';
        $text = str_replace('省', '', $text);
        $text = str_replace('市', '', $text);
        $text = str_replace('壮族自治区', '', $text);
        $text = str_replace('回族', '', $text);
        $text = str_replace('自治区', '', $text);
        $text = str_replace('特别行政区', '', $text);
        return $text;
    }

    public
    static function mobile_text($mobile)
    {
        if ($mobile) $mobile = substr_replace($mobile, '****', 3, 4);
        return $mobile;
    }

    public
    static function convert($num)
    {
        $num = round($num);
        if ($num >= 100000000) {
            $num = round($num / 100000000, 2) . '亿';
        } else if ($num >= 10000) {
            $num = round($num / 10000, 2) . '万';
        } else if ($num >= 1000) {
            $num = round($num / 1000, 2) . 'k';
        }
        return $num;
    }
}
