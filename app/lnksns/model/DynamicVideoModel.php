<?php

declare(strict_types=1);

namespace app\lnksns\model;

use lite\model\BaseModel;
use lite\service\FileService;

class DynamicVideoModel extends BaseModel
{
    protected $name = 'sns_dynamic_video';

    protected $type = [

    ];

    protected $json = [];    // 自动 json 转换


}
