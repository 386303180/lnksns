<?php

declare(strict_types=1);

namespace app\lnksns\model;

use lite\model\BaseModel;

class FeedbankModel extends BaseModel
{
    protected $name = 'sns_feedback';

    protected $type = [];

    protected $json = [];    // 自动 json 转换

    public function getImgsAttr($value)
    {
        return $value ? explode(',', $value) : [];
    }

    public function user()
    {
        return $this->hasOne(UserModel::class, "id", "user_id")->field("id,name,avatar");
    }
}
