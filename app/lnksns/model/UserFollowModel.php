<?php

declare(strict_types=1);

namespace app\lnksns\model;

use lite\model\BaseModel;


class UserFollowModel extends BaseModel
{
    protected $name = 'sns_user_follow';

    protected $type = [
    ];
    protected $json = [];    // 自动 json 转换

}
