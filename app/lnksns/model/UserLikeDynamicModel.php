<?php

declare(strict_types=1);

namespace app\lnksns\model;

use lite\model\BaseModel;
use lite\service\FileService;

class UserLikeDynamicModel extends BaseModel
{
    protected $name = 'sns_user_like_dynamic';

    protected $type = [

    ];

    protected $json = [];    // 自动 json 转换


}
