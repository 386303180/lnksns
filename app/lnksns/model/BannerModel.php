<?php

declare(strict_types=1);

namespace app\lnksns\model;

use lite\model\BaseModel;

class BannerModel extends BaseModel
{
    protected $name = 'sns_dynamic_banner';

    protected $type = [];

    protected $json = [];    // 自动 json 转换

}
