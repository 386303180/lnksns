<?php

use think\facade\Route;


Route::group('/api',function(){
    // 测试
    Route::get('/index/test','api.Index/test');
    // 登录
    Route::post('/user/wx_empower','api.User/wx_empower');
    // 配置
    Route::get('/index/config','api.Index/config');
    // 条款
    Route::get('/clause/details','api.Clause/details');
});

Route::group('/api',function(){
    
    // 反馈
    Route::post('/index/save_feedback', 'api.Index/save_feedback');
    // 用户积分
    Route::get('/integral/user_integeal_record', 'api.Integral/user_integeal_record');
    // 签到打卡
    Route::post('/integral/user_sign_in', 'api.Integral/user_sign_in');
    // 评论动态
    Route::post('/comment/comment_dynamic','api.Comment/comment_dynamic');
    // 动态评论
    Route::get('/comment/dynamic_comment','api.Comment/dynamic_comment');
    // 评论的评论
    Route::get('/comment/son_comment','api.Comment/son_comment');
    // 删除评论
    Route::post('/comment/del_comment','api.Comment/del_comment');
    // 未读消息数量
    Route::get('/message/get_message_count','api.Message/get_message_count');
    // 消息列表
    Route::get('/message/get_message','api.Message/get_message');
    // 读消息
    Route::post('/message/read_message','api.Message/read_message');
    // 喜欢动态
    Route::post('/dynamic/like_dynamic','api.Dynamic/like_dynamic');
    // 动态详情
    Route::get('/dynamic/dynamic_details','api.Dynamic/dynamic_details');
    // 动态数据
    Route::get('/dynamic/get_dynamic_info','api.Dynamic/get_dynamic_info');
    // 推荐动态
    Route::get('/dynamic/recommend_dynamic','api.Dynamic/recommend_dynamic');
    // 删除动态
    Route::post('/dynamic/del_dynamic','api.Dynamic/del_dynamic');
    // 编辑动态
    Route::post('/dynamic/save_dynamic','api.Dynamic/save_dynamic');
    // 用户圈子
    Route::get('/circle/user_circle','api.Circle/user_circle');
    // 关注圈子
    Route::post('/circle/follow_circle','api.Circle/follow_circle');
    // 随机动态推送
    Route::get('/index/rand_dynamic', 'api.Index/rand_dynamic');
    // 圈子动态
    Route::get('/circle/get_circle_dynamic','api.Circle/get_circle_dynamic');
    // 动态圈子
    Route::get('/circle/dynamic_circle','api.Circle/dynamic_circle');
    // 顶部推荐圈子
    Route::get('/circle/get_top_circle','api.Circle/get_top_circle');
    // 推荐圈子列表
    Route::get('/circle/get_circle_list','api.Circle/get_circle_list');
    // 圈子详情
    Route::get('/circle/get_circle_details','api.Circle/get_circle_details');
    // 圈子粉丝
    Route::get('/circle/circle_fans','api.Circle/circle_fans');
    // 用户详情
    Route::get('/user/user_details','api.User/user_details');
    // 用户数据
    Route::get('/user/user_publish_content','api.User/user_publish_content');
    // 关注用户
    Route::post('/user/follow_user','api.User/follow_user');
    // 用户关注数据
    Route::get('/user/user_follow','api.User/user_follow');
    // 用户刷新IP
    Route::post('/user/user_refresh_ip','api.User/user_refresh_ip');
    // 用户绑定手机号
    Route::post('/user/user_bind_mobile','api.User/user_bind_mobile');
    // 刷新用户资料
    Route::get('/user/user_refresh_info','api.User/user_refresh_info');
    // 编辑用户资料
    Route::post('/user/edit_user_info','api.User/edit_user_info');
    // 用户通知
    Route::get('/message/user_message_count','api.Message/user_message_count');
    // 上传图片
    Route::post('/upload/image','api.Upload/image');
    // 聚合搜索数量
    Route::get('/index/search_count', 'api.Index/search_count');
    // 聚合搜索
    Route::get('/index/search', 'api.Index/search');
    // 上传视频
    Route::post('/upload/video', 'api.Upload/video');
})->middleware(\app\lnksns\middleware\Check::class);




