<?php

namespace app\lnksns\controller;

use app\lnksns\model\MessageModel;
use app\lnksns\model\UserModel;
use lite\controller\Backend;
use lite\controller\traits\Crud;
use think\Request;

class Message extends Backend
{
    use Crud;

    public function initialize()
    {
        $this->model = new MessageModel();
    }

    public function index(Request $request)
    {
        $params = $request->get();

        if (!empty($request->param('page_size'))) {       // 使用分页
            $query = $this->model->with('user');
            if ($params['content']) $query = $query->where('content', 'like', '%' . $params['content'] . '%');

            $list = $query->order('id', 'desc')->paginate($request->param('page_size', 10));
        } else {
            $list = $this->model->order('id', 'desc')->select();               // 查询全部
        }

        return success('获取成功', $list);
    }

    public function save(Request $request)
    {
        $params = $request->all();
        if (count($params['user_ids']) <= 0) {
            $params['user_ids'] = UserModel::where('status', 1)->column('id');
        }
        foreach ($params['user_ids'] as $v) {
            (new MessageModel())->send($v, $params['title'], $params['content'], 0, $params['img']??'', '', $params['content_url']??'', 0);
        }
        return success('推送成功');
    }



}
