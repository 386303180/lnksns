<?php

namespace app\lnksns\controller;

use app\lnksns\model\IntegralModel;
use lite\controller\Backend;
use lite\controller\traits\Crud;
use think\Request;

class Integral extends Backend
{
    use Crud;

    public function initialize()
    {
        $this->model = new IntegralModel();
    }

    public function index(Request $request)
    {
        $params = $request->get();

        if (!empty($request->param('page_size'))) {       // 使用分页
            $query = $this->model->with('user');
            if ($params['title']) $query = $query->where('title', 'like', '%' . $params['title'] . '%');

            $list = $query->order('id', 'desc')->paginate($request->param('page_size', 10));
        } else {
            $list = $this->model->order('id', 'desc')->select();               // 查询全部
        }

        return success('获取成功', $list);
    }

}
