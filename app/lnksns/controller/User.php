<?php

namespace app\lnksns\controller;

use app\lnksns\model\UserModel;
use lite\controller\Backend;
use lite\controller\traits\Crud;
use think\Request;

class User extends Backend
{
    use Crud;

    public function initialize()
    {
        $this->model = new UserModel();
    }

    public function index(Request $request)
    {
        $params = $request->get();

        if (!empty($request->param('page_size'))) {       // 使用分页
            $query = $this->model;
            if ($params['name']) $query = $query->where('name', 'like', '%' . $params['name'] . '%');
            if ($params['mobile']) $query = $query->where('mobile', '=', $params['mobile']);

            $list = $query->paginate($request->param('page_size', 10));
        } else {
            $list = $this->model->select();               // 查询全部
        }

        return success('获取成功', $list);
    }


    public function select_list()
    {
        $list = $this->model->where('status', 1)
            ->field('id,name')
            ->select();

        return success('获取成功', $list);
    }

}
