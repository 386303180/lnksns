<?php

declare(strict_types=1);

namespace app\lnksns\controller;

use app\Request;
use lite\controller\Backend;
use lite\service\ConfigService;

class Config extends Backend
{
    public function detail()
    {
        $data = cache('webconfig');
        if(empty($data)){
            $data = [
                'lnk_app_id' => ConfigService::get('lnksns', 'lnk_app_id',''),
                'lnk_app_secret' => ConfigService::get('lnksns', 'lnk_app_secret',''),
                'lnk_gw' => ConfigService::get('lnksns', 'lnk_gw','https://www.lnksns.vip'),
                'lnk_gzh' => ConfigService::get('lnksns', 'lnk_gzh','https://mp.weixin.qq.com/'),
                'lnk_tx_key' => ConfigService::get('lnksns', 'lnk_tx_key',''),
                'lnk_nl' => ConfigService::get('lnksns', 'lnk_nl',[]),
                'lnk_zy' => ConfigService::get('lnksns', 'lnk_zy',[]),
                'lnk_jf_open' => (int) ConfigService::get('lnksns', 'lnk_jf_open',0),
                'lnk_jf_qd_value' => (int) ConfigService::get('lnksns', 'lnk_jf_qd_value',10),
                'lnk_jf_new_user' => (int) ConfigService::get('lnksns', 'lnk_jf_new_user',0),
                'lnk_jb' => ConfigService::get('lnksns', 'lnk_jb',[]),
                'lnk_city' => ConfigService::get('lnksns', 'lnk_city','上海'),
                'lnk_sh_dt' => (int) ConfigService::get('lnksns', 'lnk_sh_dt',0),
                'lnk_sh_pl' => (int) ConfigService::get('lnksns', 'lnk_sh_pl',0),

            ];
            cache('webconfig',$data);
        }
        return success($data);
    }

    public function update(Request $request)
    {

        ConfigService::set('lnksns','lnk_app_id',$request->post('lnk_app_id',''));
        ConfigService::set('lnksns','lnk_app_secret',$request->post('lnk_app_secret',''));
        ConfigService::set('lnksns','lnk_gw',$request->post('lnk_gw',''));
        ConfigService::set('lnksns','lnk_gzh',$request->post('lnk_gzh',''));
        ConfigService::set('lnksns','lnk_tx_key',$request->post('lnk_tx_key',''));
        ConfigService::set('lnksns','lnk_nl',$request->post('lnk_nl',[]));
        ConfigService::set('lnksns','lnk_zy',$request->post('lnk_zy',[]));

        ConfigService::set('lnksns','lnk_jf_open',$request->post('lnk_jf_open',0));
        ConfigService::set('lnksns','lnk_jf_qd_value',$request->post('lnk_jf_qd_value',10));
        ConfigService::set('lnksns','lnk_jf_new_user',$request->post('lnk_jf_new_user',0));
        ConfigService::set('lnksns','lnk_jb',$request->post('lnk_jb',[]));
        ConfigService::set('lnksns','lnk_city',$request->post('lnk_city','上海'));
        ConfigService::set('lnksns','lnk_sh_dt',$request->post('lnk_sh_dt',0));
        ConfigService::set('lnksns','lnk_sh_pl',$request->post('lnk_sh_pl',0));
        cache('webconfig',null);
        cache('wapconfig',null);
        
        return success();
    }

}
