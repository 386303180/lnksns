<?php

namespace app\lnksns\controller;

use app\lnksns\model\BannerModel;
use app\lnksns\model\DynamicModel;
use app\lnksns\model\DynamicImgModel;
use app\lnksns\model\DynamicVideoModel;
use app\lnksns\model\UserModel;
use lite\controller\Backend;
use lite\controller\traits\Crud;
use think\facade\Db;
use think\Request;

class Banner extends Backend
{
    use Crud;

    public function initialize()
    {
        $this->model = new BannerModel();
    }

    public function index(Request $request)
    {
        $params = $request->get();

        if (!empty($request->param('page_size'))) {       // 使用分页
            $query = $this->model;
            if ($params['content']) $query = $query->where('content', 'like', '%' . $params['content'] . '%');

            $list = $query->order('id', 'desc')->paginate($request->param('page_size', 10));
        } else {
            $list = $this->model->order('id', 'desc')->select();               // 查询全部
        }

        return success('获取成功', $list);
    }

    public function save(Request $request)
    {
        $params = $request->all();
        if (!$params['dynamic_id']) return error('请选择动态再保存');
        $dynamic_info = DynamicModel::where('id', $params['dynamic_id'])->field('id,content,user_id,type')->find();
        if ($dynamic_info['type'] == 1) {
            $dynamic_info['img'] = DynamicImgModel::where('dynamic_id', $dynamic_info['id'])->order('weigh', 'asc')->value('url');
        } else if ($dynamic_info['type'] == 1) {
            $dynamic_info['img'] = DynamicVideoModel::where('dynamic_id', $dynamic_info['id'])->value('img');
        }
        $user_info = UserModel::where('id', $dynamic_info['user_id'])->field('name,avatar')->find();
        $this->model->create([
            'dynamic_id' => $dynamic_info['id'],
            'user_id' => $dynamic_info['user_id'],
            'user_name' => $user_info['name'],
            'user_avatar' => $user_info['avatar'],
            'content' => $dynamic_info['content'],
            'img' => $dynamic_info['img'],
            'weigh' => 999,//$params['weigh'],
            'status' => $params['status'],
        ]);
        return success('添加成功');
    }

    public function update(Request $request)
    {
        $params = $request->all();
        if (!$params['dynamic_id']) return error('请选择动态再保存');
        $dynamic_info = DynamicModel::where('id', $params['dynamic_id'])->field('id,content,user_id,type')->find();
        if ($dynamic_info['type'] == 1) {
            $dynamic_info['img'] = DynamicImgModel::where('dynamic_id', $dynamic_info['id'])->order('weigh', 'asc')->value('url');
        } else if ($dynamic_info['type'] == 1) {
            $dynamic_info['img'] = DynamicVideoModel::where('dynamic_id', $dynamic_info['id'])->value('img');
        }
        $user_info = UserModel::where('id', $dynamic_info['user_id'])->field('name,avatar')->find();
        $this->model->where('id', $params['id'])->update([
            'dynamic_id' => $dynamic_info['id'],
            'user_id' => $dynamic_info['user_id'],
            'user_name' => $user_info['name'],
            'user_avatar' => $user_info['avatar'],
            'content' => $dynamic_info['content'],
            'img' => $dynamic_info['img'],
            'weigh' => $params['weigh'],
            'status' => $params['status'],
        ]);
        return success('保存成功');
    }
}
