<?php
declare(strict_types=1);

namespace app\lnksns\controller\api;


use lite\service\UploadService;
use think\Request;

class Upload
{
    /**
     * 上传图片
     */
    public function image(Request $request)
    {
        $file = UploadService::image($request->post('group_id',0));
        return success('上传成功',$file);
    }

    /**
     * 上传视频
     */
    public function video(Request $request)
    {
        $file = UploadService::video($request->post('group_id',0));
        
        return success('上传成功',$file);
    }
}