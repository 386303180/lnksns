<?php

namespace app\lnksns\controller\api;

use app\lnksns\model\CircleModel;
use app\lnksns\model\CircleFansModel;
use app\lnksns\model\DynamicModel;
use app\lnksns\model\UserModel;
use app\lnksns\service\CircleService;
use app\lnksns\service\DynamicService;
use think\Request;
use app\appcenter\lib\AppIHookService;

class Circle
{

    public function get_top_circle(Request $request)
    {
        $data = CircleModel::where('status', 1)
            ->field('id,name,highlight')
            ->limit(15)
            ->select()->toArray();
        foreach ($data as $k => $v) {
            $data[$k]['user'] = [];
            $data[$k]['user_count'] = CircleFansModel::where('circle_id', $v['id'])->count();
            if ($data[$k]['user_count']) {
                $uids = (new CircleFansModel())
                    ->where('circle_id', $v['id'])
                    ->limit(3)
                    ->column('user_id');
                $data[$k]['user'] = UserModel::whereIn('id', $uids)->column('avatar');
            }
        }
        $data = array_chunk($data, 5);

        return success('成功', $data);
    }

    public function get_circle_list(Request $request)
    {
        $data = CircleModel::where('status', 1)
            ->order('weigh', 'desc')
            ->order('create_time', 'desc')
            ->field('id,avatar,name')
            ->paginate(6)->toArray();
        $data = CircleService::circle_map($data);

        return success('成功', $data);
    }

    public function get_circle_details(Request $request)
    {
        $uid = $request->uid;
        $id = $request->get('id');
        $data = CircleModel::where('status', 1)
            ->where('id', $id)
            ->field('id,avatar,name,intro,integral,type')
            ->find();
        if ($data) {
            $data['is_follow'] = CircleFansModel::where('user_id', $uid)
                ->where('circle_id', $data['id'])
                ->count() ? true : false;

            $data['user'] = [];
            $data['user_count'] = CircleFansModel::where('circle_id', $data['id'])->count();
            if ($data['user_count']) {
                $uids = CircleFansModel::where('circle_id', $data['id'])
                    ->limit(3)
                    ->column('user_id');
                $data['user'] = UserModel::whereIn('id', $uids)->column('avatar');
            }
            return success('成功', $data);
        } else {
            return error('未找到圈子或圈子异常！', 400);
        }
    }

    public function get_circle_dynamic(Request $request)
    {
        $uid = $request->uid;
        $id = $request->get('id');
        $type = $request->get('type', 0);
        $order = ['top' => 'desc', 'create_time' => 'desc', 'weigh' => 'desc', 'view' => 'desc'];
        if ($type == 1) $order = ['create_time' => 'desc', 'weigh' => 'desc', 'view' => 'desc'];
        if ($type == 2) $order = ['weigh' => 'desc', 'create_time' => 'desc', 'view' => 'desc'];
        $data = DynamicModel::where('status', 1)
            ->where('show', 1)
            ->where('circle_id', $id)
            ->order($order)
            ->field(DynamicService::dynamic_find())
            ->paginate(6)->toArray();
        $data = DynamicService::dynamic_map($data, true, $uid);

        return success('成功', $data);
    }

    //关注圈子
    public function follow_circle(Request $request)
    {
        $uid = $request->uid;
        $id = $request->post('id');
        $is_follow = $request->post('is_follow');
        if ($is_follow) {
            $weigh = CircleFansModel::where('user_id', $uid)->where('circle_id', $id)->value('weigh');
            if ($weigh == 999) return error('您是圈子创建者不可以退出圈子');
            CircleFansModel::where('user_id', $uid)
                ->where('circle_id', $id)
                ->delete();
        } else {
            AppIHookService::Hook_Run('follow_circle',$data,['uid'=>$uid,'cid'=>$id]);
            CircleFansModel::insert([
                'user_id' => $uid,
                'circle_id' => $id,
                'create_time' => time()
            ]);
            AppIHookService::Hook_Run('follow_circle_out',$data,['uid'=>$uid,'cid'=>$id]);
        }
        return success('成功', '');
    }
    // 用户圈子
    public function user_circle(Request $request)
    {
        $uid = $request->uid;
        $user_id = $request->get('user_id', 0);
        if ($user_id > 0) $uid = $user_id;
        $data = CircleFansModel::where('user_id', $uid)
            ->order('id', 'desc')
            ->field('id,circle_id')
            ->select()->toArray();
        if (count($data)) {
            foreach ($data as $k => $v) {
                $circle = CircleModel::where('id', $v['circle_id'])
                    ->field('name,avatar')
                    ->find();
                $circle['is_new'] = DynamicModel::where('circle_id', $v['circle_id'])
                    ->where('create_time', '>', strtotime(date('Y-m-d')))
                    ->count() ? true : false;
                $data[$k]['circle'] = $circle;
            }
        }

        return success('成功', $data);
    }

    public function circle_fans(Request $request)
    {
        $id = $request->get('id');
        $data = CircleFansModel::where('circle_id', $id)
            ->field('id,user_id')
            ->paginate(40)->toArray();
        if (count($data['data'])) {
            foreach ($data['data'] as $k => $v) {
                $data['data'][$k]['user'] = UserModel::where('id', $v['user_id'])
                    ->field('name,avatar')
                    ->find();
            }
        }

        return success('成功', $data);
    }

    public function dynamic_circle(Request $request)
    {
        $uid = $request->uid;
        $ids = CircleFansModel::where('user_id', $uid)->column('circle_id');
        if (count($ids)) {
            $data = CircleModel::where('status', 1)
                ->whereIn('id', $ids)
                ->order('weigh', 'desc')
                ->field('id,name,avatar')
                ->select();
        }
        return success('成功', $data ?? []);
    }





}
