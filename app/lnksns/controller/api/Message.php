<?php

namespace app\lnksns\controller\api;

use app\lnksns\lib\SuPengJun;
use app\lnksns\model\MessageModel;
use app\lnksns\model\UserModel;
use think\Request;

class Message
{

    public function user_message_count(Request $request)
    {
        $uid = $request->uid;
        $data = MessageModel::where('user_id', $uid)->where('read', 0)->count();
        return success('success', $data);
    }

    public function get_message(Request $request)
    {
        $uid = $request->uid;
        $type = $request->get('type');
        $data = MessageModel::where('user_id', $uid)
            ->where('type', $type)
            ->order('read','asc')
            ->order('id','desc')
            ->field('id,launch_id,title,content,img,avatar_url,content_url,read,create_time')
            ->paginate(8)->toArray();
        if (count($data['data'])) {
            foreach ($data['data'] as $k => $v) {
                if ($v['launch_id']) {
                    $data['data'][$k]['user'] = UserModel::where('id', $v['launch_id'])->field('name,avatar')->find();
                }
                $data['data'][$k]['create_time'] = SuPengJun::time_text($v['create_time']);
            }
        }

        return success('success', $data);
    }

    public function get_message_count(Request $request)
    {
        $uid = $request->uid;
        $data[0] = MessageModel::where('user_id', $uid)->where('type', 0)->where('read', 0)->count();
        $data[1] = MessageModel::where('user_id', $uid)->where('type', 1)->where('read', 0)->count();
        $data[2] = MessageModel::where('user_id', $uid)->where('type', 2)->where('read', 0)->count();

        return success('success', $data);
    }

    public function read_message(Request $request)
    {
        $uid = $request->uid;
        $type = $request->post('type');
        $id = $request->post('id', 0);
        $condition[] = ['user_id', '=', $uid];
        if ($id) $condition[] = ['id', '=', $id];
        MessageModel::where('type', $type)
            ->where($condition)
            ->update(['read' => 1]);

        return success('success', '');
    }

}
