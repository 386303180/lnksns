<?php

namespace app\lnksns\controller\api;

use lite\service\ConfigService;
use app\lnksns\model\IntegralModel;
use app\lnksns\model\UserModel;
use app\lnksns\service\UserService;
use think\Request;

class Integral
{

    public function user_sign_in(Request $request)
    {
        $uid = $request->uid;
        $exists = IntegralModel::where('user_id', $uid)
            ->where('create_time', '>=', strtotime(date('Y-m-d')))
            ->where('title','打卡签到')
            ->count() ? true : false;
        if ($exists) {
            return error('您今天已经签到啦！明天再来吧', 500801);
        } else {
            $lnk_jf_qd_value = ConfigService::get('lnksns', 'lnk_jf_qd_value', 0);
            if($lnk_jf_qd_value) (new IntegralModel())->integral_save($uid, '打卡签到', $lnk_jf_qd_value, 1);
            $data = UserModel::where('id', $uid)->field(UserService::user_find())->find();
            $data = UserService::user_other($data);
            return success('🎉 打卡成功，明日打卡继续领',$data);
        }
    }

    public function user_integeal_record(Request $request)
    {
        $uid = $request->uid;
        $data = IntegralModel::where('user_id', $uid)
            ->field('id,title,number,type,create_time')
            ->order('id', 'desc')
            ->paginate(20);

        return success('成功',$data);
    }

}
