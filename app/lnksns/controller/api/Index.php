<?php
declare(strict_types=1);

namespace app\lnksns\controller\api;

use app\lnksns\model\BannerModel;
use app\lnksns\model\CircleModel;
use app\lnksns\model\DynamicModel;
use app\lnksns\model\UserModel;
use app\lnksns\model\FeedbankModel;
use app\lnksns\model\UserFollowModel;
use app\lnksns\service\CircleService;
use app\lnksns\service\DynamicService;
use lite\service\ConfigService;
use think\facade\Db;
use think\Request;
use app\appcenter\lib\AppIHookService;

class Index
{
    // public function test()
    // {
    //     return phpinfo();
    // }

    public function config()
    {
        $data = cache('wapconfig');
        if(empty($data))
        {
            $data = [
                'lnk_gw' => ConfigService::get('lnksns', 'lnk_gw','https://www.lnksns.vip'),
                'lnk_gzh' => ConfigService::get('lnksns', 'lnk_gzh','https://mp.weixin.qq.com/'),
                'lnk_tx_key' => ConfigService::get('lnksns', 'lnk_tx_key',''),
                'lnk_nl' => ConfigService::get('lnksns', 'lnk_nl',[]),
                'lnk_zy' => ConfigService::get('lnksns', 'lnk_zy',[]),
                'lnk_jf_open' => (int) ConfigService::get('lnksns', 'lnk_jf_open',0),
                'lnk_jf_qd_value' => (int) ConfigService::get('lnksns', 'lnk_jf_qd_value',10),
                'lnk_jb' => ConfigService::get('lnksns', 'lnk_jb',[]),
                'lnk_city' => ConfigService::get('lnksns', 'lnk_city','上海'),
            ];
            cache('wapconfig',$data);
        }
        AppIHookService::Hook_Run('wap_config',$data);
        return success('success', $data);
    }

    public function save_feedback(Request $request)
    {
        $uid = $request->uid;
        $params = $request->post();
        FeedbankModel::insert([
            'user_id' => $uid,
            'contact' => $params['contact'] ?? '',
            'type' => $params['type'],
            'content' => $params['content'],
            'imgs' => $params['imgs'] ? implode(',', $params['imgs']) : '',
            'status' => 0,
            'create_time' => time(),
        ]);
        return success('提交成功，我们会在24h内为您处理。', '');
    }

    public function rand_dynamic()
    {
        $data = BannerModel::where('status', 1)
            ->orderRaw('RAND()')
            ->field('id,dynamic_id,user_id,user_name,user_avatar,content,img')
            ->find();
        return success('成功', $data);
    }

    public function search_count(Request $request)
    {
        $keyword = $request->get('keyword');
        $condition[] = ['status', '=', 1];
        $data[0] = DynamicModel::where($condition)
            ->where('show', 1)
            ->where('content|circle_name|province|city|district|adds', 'like', '%' . $keyword . '%')
            ->count();
        $data[1] = CircleModel::where($condition)
            ->where('name|intro|province|city|district', 'like', '%' . $keyword . '%')
            ->count();
        $data[2] = UserModel::where($condition)
            ->where('name|career', 'like', '%' . $keyword . '%')
            ->count();

        return success('成功', $data);
    }

    public function search(Request $request)
    {
        $uid = $request->uid;
        $type = $request->get('type', 1);
        $keyword = $request->get('keyword');
        $condition[] = ['status', '=', 1];
        if ($type == 0) {
            $condition[] = ['show', '=', 1];
            $condition[] = ['content|circle_name|province|city|district|adds', 'like', '%' . $keyword . '%'];
            $data = DynamicModel::where($condition)
                ->order('weigh', 'desc')
                ->order('create_time', 'desc')
                ->field(DynamicService::dynamic_find())
                ->paginate(6)->toArray();
            $data = DynamicService::dynamic_map($data, true, $uid);
        } else if ($type == 1) {
            $condition[] = ['name|intro|province|city|district', 'like', '%' . $keyword . '%'];
            $data = CircleModel::where($condition)
                ->order('weigh', 'desc')
                ->order('create_time', 'desc')
                ->field('id,avatar,name')
                ->paginate(6)->toArray();
            $data = CircleService::circle_map($data);
        } else if ($type == 2) {
            $condition[] = ['name|career', 'like', '%' . $keyword . '%'];
            $data = UserModel::where($condition)
                ->order('weigh', 'desc')
                ->order('create_time', 'desc')
                ->field('id,avatar,name,career')
                ->paginate(12)->toArray();
            foreach ($data['data'] as $k => $v) {
                $data['data'][$k]['is_follow'] = UserFollowModel::where('user_id', $uid)
                    ->where('follow_user_id', $v['id'])
                    ->count() ? true : false;
            }
        }

        return success('成功', $data);
    }
}