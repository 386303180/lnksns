<?php

namespace app\lnksns\controller\api;

use app\lnksns\model\ClauseModel;
use think\Request;

class Clause
{

    public function details(Request $request)
    {
        $id = $request->get('id');
        $data = ClauseModel::where('id', $id)->where('status',1)->field('id,title,content')->find();
        return success('success',$data);
    }

}
