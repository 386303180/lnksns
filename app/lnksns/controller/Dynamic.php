<?php

namespace app\lnksns\controller;

use app\lnksns\model\CommentModel;
use app\lnksns\model\DynamicModel;
use app\lnksns\model\DynamicImgModel;
use app\lnksns\model\DynamicVideoModel;
use app\lnksns\model\MessageModel;
use app\lnksns\model\UserLikeDynamicModel;
use lite\controller\Backend;
use lite\controller\traits\Crud;
use think\facade\Db;
use think\Request;

class Dynamic extends Backend
{
    use Crud;

    public function initialize()
    {
        $this->model = new DynamicModel();
    }

    public function index(Request $request)
    {
        $params = $request->get();

        if (!empty($request->param('page_size'))) {       // 使用分页
            $query = $this->model->with('user')->append(['like_count', 'comment_count'])
                ->withAttr('like_count', function ($value, $data) {
                    return UserLikeDynamicModel::where('dynamic_id', $data['id'])->count();
                })->withAttr('comment_count', function ($value, $data) {
                    return CommentModel::where('status', '<>', 0)->where('dynamic_id', $data['id'])->count();
                });
            if ($params['content']) $query = $query->where('content', 'like', '%' . $params['content'] . '%');
            if ($params['id']) $query = $query->where('id', $params['id']);
            if ($params['type'] >= 0) $query = $query->where('type', $params['type']);
            if ($params['status'] >= 0) $query = $query->where('status', $params['status']);

            $list = $query->order(['status' => 'asc', 'top' => 'desc', 'create_time' => 'desc', 'view' => 'desc'])->paginate($request->param('page_size', 10));
        } else {
            $list = $this->model->order('status', 'asc')->order('weigh', 'desc')->order('id', 'desc')->select();               // 查询全部
        }

        return success('获取成功', $list);
    }

    public function read($id)
    {
        $detail = $this->model->findOrFail($id);
        if ($detail->type == 1) $detail->imgs = DynamicImgModel::where('dynamic_id', $id)
            ->order('weigh', 'asc')
            ->field('url,wide,high')
            ->select();
        if ($detail->type == 2) $detail->video = DynamicVideoModel::where('dynamic_id', $id)
            ->field('img,url,wide,high')
            ->find();
        return success('获取成功', $detail);
    }

    public function update(Request $request)
    {
        $params = $request->all();
        // 发送驳回消息通知用户
        if ($params['status'] == 3 && $params['reason']) {
            $content = "驳回原因：" . $params['reason'] . "；您可以编辑后重发布";
            if ($params['type'] == 1) {
                $img = DynamicImgModel::where('dynamic_id', $params['id'])->order('weigh', 'asc')->value('url');
            } else if ($params['type'] == 2) {
                $img = DynamicVideoModel::where('dynamic_id', $params['id'])->value('img');
            }
            $content_url = '/pages/dynamic/details?id=' . $params['id'];
            (new MessageModel())->send($params['user_id'], '动态被驳回', $content, 1, $img ?? '', '', $content_url);
        }
        $this->model->where('id', $params['id'])->update([
            'top' => $params['top'],
            'show' => $params['show'],
            'weigh' => $params['weigh'],
            'status' => $params['status'],
        ]);
        return success('保存成功');
    }

    public function delete($id)
    {
        $pk = $this->model->getPk();

        $result = Db::transaction(function () use ($id, $pk) {
            $count = 0;
            foreach ($this->model->whereIn($pk, $id)->cursor() as $row) {
                $count += $row->delete();
                // 删除用户关注的动态
                UserLikeDynamicModel::where('dynamic_id', $id)->delete();
            }
            return $count;
        });
        if ($result) {
            return success('删除成功', $result);
        }
        return error('删除失败');
    }

    public function select_list()
    {
        $list = $this->model->where('status', 1)
            ->where('type', '<>', 0)
            ->order('id', 'desc')
            ->field('id,content')
            ->select();

        return success('获取成功', $list);
    }
}
