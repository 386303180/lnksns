<?php

namespace app\lnksns\controller;

use app\lnksns\model\CommentModel;
use lite\controller\Backend;
use lite\controller\traits\Crud;

use think\Request;

class Comment extends Backend
{
    use Crud;

    public function initialize()
    {
        $this->model = new CommentModel();
    }

    public function index(Request $request)
    {
        $params = $request->get();

        if (!empty($request->param('page_size'))) {       // 使用分页
            $query = $this->model->with('user');
            if ($params['content']) $query = $query->where('content', 'like', '%' . $params['content'] . '%');
            if ($params['status'] >= 0) $query = $query->where('status', $params['status']);

            $list = $query->order('status', 'asc')->order('id', 'desc')->paginate($request->param('page_size', 10));
        } else {
            $list = $this->model->order('status', 'asc')->order('id', 'desc')->select();               // 查询全部
        }

        return success('获取成功', $list);
    }
}