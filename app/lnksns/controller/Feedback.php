<?php

namespace app\lnksns\controller;

use app\lnksns\model\CircleModel;
use app\lnksns\model\FeedbankModel;
use lite\controller\Backend;
use lite\controller\traits\Crud;
use think\Request;

class Feedback extends Backend
{
    use Crud;

    public function initialize()
    {
        $this->model = new FeedbankModel();
    }

    public function index(Request $request)
    {
        $params = $request->get();

        if (!empty($request->param('page_size'))) {       // 使用分页
            $query = $this->model->with('user');
            if ($params['content']) $query = $query->where('type|content', 'like', '%' . $params['content'] . '%');

            $list = $query->order('status', 'asc')->order('id', 'desc')->paginate($request->param('page_size', 10));
        } else {
            $list = $this->model->order('status', 'asc')->order('id', 'desc')->select();               // 查询全部
        }

        return success('获取成功', $list);
    }

}
