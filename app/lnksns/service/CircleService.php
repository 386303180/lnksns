<?php

namespace app\lnksns\service;

use app\lnksns\lib\SuPengJun;
use app\lnksns\model\CommentModel;
use app\lnksns\model\DynamicModel;
use app\lnksns\model\DynamicImgModel;

use app\lnksns\model\UserModel;
use app\lnksns\model\UserLikeDynamicModel;
use app\lnksns\model\CircleFansModel;
use think\facade\Db;

class CircleService
{

    public static function circle_map($data)
    {
        $list = $data['data'];
        foreach ($list as $k => $v) {
            $list[$k]['user_count'] = CircleFansModel::where('circle_id', $v['id'])->count();
            $list[$k]['dynamic_count'] = DynamicModel::where('circle_id', $v['id'])->count();
            $list[$k]['is_new'] = DynamicModel::where('circle_id', $v['id'])
                ->where('create_time', '>', strtotime(date('Y-m-d')))
                ->count() ? true : false;
            $dynamic = DynamicModel::where('status', 1)
                ->where('show', 1)
                ->where('circle_id', $v['id'])
                ->field('id,user_id,content')
                ->limit(2)
                ->select()->toArray();
            if (count($dynamic)) {
                foreach ($dynamic as $dk => $dv) {
                    $dynamic[$dk]['user'] = UserModel::where('id', $dv['user_id'])->field('name,avatar,career')->find();
                    $dynamic[$dk]['dynamic_comment'] = CommentModel::where('dynamic_id', $dv['id'])->count();
                    $dynamic[$dk]['dynamic_like'] = UserLikeDynamicModel::where('dynamic_id', $dv['id'])->count();
                    $dynamic[$dk]['dynamic_img'] = DynamicImgModel::where('dynamic_id', $dv['id'])->count();
                    if ($dynamic[$dk]['dynamic_img']) {
                        $dynamic[$dk]['img'] = DynamicImgModel::where('dynamic_id', $dv['id'])
                            ->order('weigh', 'asc')
                            ->value('url');
                    }
                }
            }
            $list[$k]['dynamic'] = $dynamic ?? [];
        }
        $data['data'] = $list;
        return $data;
    }

}
