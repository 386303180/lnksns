<?php

namespace app\lnksns\service;

use app\lnksns\lib\SuPengJun;
use app\lnksns\model\CommentModel;
use app\lnksns\model\DynamicImgModel;
use app\lnksns\model\DynamicVideoModel;
use app\lnksns\model\UserModel;
use app\lnksns\model\UserLikeDynamicModel;
use think\facade\Db;

class DynamicService
{

    public static function dynamic_find()
    {
        return ['id,user_id,content,circle_id,circle_name,province,adds,lat,lng,create_time,view,type,status'];
    }

    public static function dynamic_map($data, $type = true, $uid = 0)
    {
        $list = $data['data'];
        foreach ($list as $k => $v) {
            $list[$k]['user'] = UserModel::where('id', $v['user_id'])
                ->field('name,avatar,career')
                ->find();
            if ($v['type'] == 1) {
                $list[$k]['img'] = DynamicImgModel::where('dynamic_id', $v['id'])
                    ->order('weigh', 'asc')
                    ->field('url,wide,high')
                    ->limit(3)
                    ->select();
                $list[$k]['img_count'] = DynamicImgModel::where('dynamic_id', $v['id'])
                    ->count();
            } else if ($v['type'] == 2) {
                $list[$k]['video'] = DynamicVideoModel::where('dynamic_id', $v['id'])
                    ->field('url,img,wide,high')
                    ->find();
            }
            $list[$k]['create_time'] = SuPengJun::time_text($v['create_time']);
            $list[$k]['comment_count'] = CommentModel::where('status', '<>', 0)->where('dynamic_id', $v['id'])->count();
            $list[$k]['like_count'] = UserLikeDynamicModel::where('dynamic_id', $v['id'])->count();
            if ($type) {
                if ($list[$k]['comment_count']) {
                    $comment = CommentModel::where('dynamic_id', $v['id'])
                        ->where('status', '<>', 0)
                        ->field('user_id,content,status')
                        ->find();
                    if (!$comment['content'] && $comment['status'] == 1) {
                        $comment['content'] = '[动画表情]';
                    } else {
                        $comment['content'] = CommentService::comment_status($comment['content'], $comment['status']);
                    }
                    $comment['user_name'] = UserModel::where('id', $comment['user_id'])->value('name');
                    $list[$k]['comment'] = $comment;
                }
                $list[$k]['is_like'] = UserLikeDynamicModel::where('dynamic_id', $v['id'])
                    ->where('user_id', $uid)
                    ->where('status', 1)
                    ->count() ? true : false;
            }
        }
        $data['data'] = $list;
        return $data;
    }

}
