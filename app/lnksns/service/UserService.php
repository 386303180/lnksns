<?php

namespace app\lnksns\service;

use app\lnksns\lib\SuPengJun;
use app\lnksns\model\DynamicModel;
use app\lnksns\model\IntegralModel;
use app\lnksns\model\UserFollowModel;
use app\lnksns\model\UserLikeDynamicModel;
use think\facade\Db;

class UserService
{

    public
    static function user_find()
    {
        return ['id,name,avatar,gender,age,career,integral_all,integral_usable,integral_used,mobile,city,province,status'];
    }

    public
    static function user_other($info, $uid = 0)
    {
        if ($uid) {
            $condition[] = ['status', '=', 1];
            $condition[] = ['show', '=', 1];
            $info['is_follow'] = UserFollowModel::where('follow_user_id', $info['id'])
                ->where('user_id', $uid)
                ->count() ? true : false;
        }else{
            $condition[] = ['status', 'in', [0, 1]];
            $info['is_clockin'] = IntegralModel::where('user_id', $info['id'])
                ->where('create_time', '>=', strtotime(date('Y-m-d')))
                ->where('title','打卡签到')
                ->count() ? true : false;
        }
        $info['follow'] = UserFollowModel::where('user_id', $info['id'])
            ->count();
        if ($info['follow'] > 999) $info['follow'] = SuPengJun::convert($info['follow']);
        $info['fans'] = UserFollowModel::where('follow_user_id', $info['id'])
            ->count();
        if ($info['fans'] > 999) $info['fans'] = SuPengJun::convert($info['fans']);
        $info['integral_usable_num'] = $info['integral_usable'];
        if ($info['integral_usable'] > 999) $info['integral_usable'] = SuPengJun::convert($info['integral_usable']);
        $info['dynamic_count'] = DynamicModel::where('user_id', $info['id'])->where($condition)->count();
        $info['like_dynamic_count'] = UserLikeDynamicModel::where('status',1)->where('user_id', $info['id'])->count();
        $info['mobile'] = SuPengJun::mobile_text($info['mobile']);
        $info['province'] = SuPengJun::adds_text($info['province']);
        $info['city'] = SuPengJun::adds_text($info['city']);
        return $info;
    }

}
