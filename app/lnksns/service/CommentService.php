<?php

namespace app\lnksns\service;


class CommentService
{

    public static function comment_status($content, $status)
    {
        if ($status == 2) {
            $content = '（评论内容违法违规）';
        } else if ($status == 3) {
            $content = '（该评论已被删除）';
        }
        return $content;
    }
}
