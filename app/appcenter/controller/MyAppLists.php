<?php
/*
 * 邻客互动 lnksns 高颜值论坛博客、社区圈子、积分商城系统 https://www.lnksns.vip
 * 邻客互动，为各类自媒体提供全面运营解决方案，助力自媒体运营成长。
 * 承接DQ、DX、DEDE、等各种PHP、SNS程序的定制开发业务,BUG修复业务！
 * 联系QQ：987501448 WX：nzkd01 
 */
/**
 *      This is NOT a freeware, use is subject to license terms
 *      应用名称: 邻客互动 应用中心
 *      应用开发者: 叶子
 *      开发者QQ: 987501448
 *      未经应用程序开发者/所有者的书面许可，不得进行反向工程、反向汇编、反向编译等，不得擅自复制、修改、链接、转载、汇编、发表、出版、发展与之有关的衍生产品、作品等
 *      版权所有: 互娱时代襄阳文化产业有限公司
 */
namespace app\appcenter\controller;

use lite\controller\Backend;
use think\Request;
class MyAppLists extends Backend
{
    //------ Admin ------
    public $class,$appinfo;
    public function __construct(Request $request)
    {
        if (!empty($request->param('app')) && !empty($request->param('model'))) { 
            $app = get_del_html(strtolower($request->param('app')));
            $model = get_del_html(strtolower($request->param('model')));
            $this->class = "app\\plugin\\".$app."\\hook\admin\hook_".$model.''::class;
            $this->appinfo = "app\\plugin\\".$app."\\\config"::class;
        }
    }

    /**
     * 添加
     *
     * @return \think\Response
     */
    public function create(Request $request)
    {
        if (!empty($request->param('app')) && !empty($request->param('model'))) { 
            
            if (method_exists($this->class,'save')) {
                $result = $this->class::save($request);
                if ($result) {
                    return success('保存成功',$result);
                }
                return error('保存失败');
            }
        }
        return error('未获取APP信息', 401);
    }

    /**
     * 编辑
     *
     * @param  $id
     * @return \think\Response
     */
    public function update(Request $request)
    {
        $params = $request->get();
        if (!empty($request->param('app')) && !empty($request->param('model'))) { 
            if (method_exists($this->class,'update')) {
                $result = $this->class::update($request,$params['id']);
                if ($result) {
                    return success('修改成功',$result);
                }
                return error('修改失败');
            }
        }
        return error('未获取APP信息', 402);
    }
    
    /**
     * 详情
     *
     * @param  $id
     * @return \think\Response
     */
    public function detail(Request $request)
    {
        $params = $request->get();
        if (!empty($request->param('app')) && !empty($request->param('model'))) { 
            
            if (method_exists($this->class,'detail')) {
                $result = $this->class::detail($request);
                if ($result) {
                    return success('获取成功',$result);
                }
                return error('获取失败');
            }
        }
        return error('未获取APP信息', 403);
    }

    

    /**
     * 删除 function
     *
     * @param Request $request
     * @return void
     */
    public function delete(Request $request)
    {
        $params = $request->get();
        if (!empty($request->param('app')) && !empty($request->param('model'))) { 
            if (method_exists($this->class,'delete')) {
                $result = $this->class::delete($params['id']);
                if ($result) {
                    return success('删除成功',$result);
                }
                return error('删除失败');
            }
        }
        return error('未获取APP信息', 404);
    }


    /**
     * 列表 function
     *
     * @param Request $request
     * @return void
     */
    public function index(Request $request)
    {
        $params = $request->get();
        if (!empty($request->param('app')) && !empty($request->param('model'))) { 
            if (method_exists($this->class,'index')) {
                $list = [];
                $list['name'] = $this->appinfo::$app_name;
                $list['field'] = $this->class::$field;
                $list['mod'] = $this->class::$mod;
                $list['lists'] = $this->class::index($request);
                return success('获取成功', $list);

            }
        }
        return error('未获取APP信息', 405);
    }

}
