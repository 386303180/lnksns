<?php
/*
 * 邻客互动 lnksns 高颜值论坛博客、社区圈子、积分商城系统 https://www.lnksns.vip
 * 邻客互动，为各类自媒体提供全面运营解决方案，助力自媒体运营成长。
 * 承接DQ、DX、DEDE、等各种PHP、SNS程序的定制开发业务,BUG修复业务！
 * 联系QQ：987501448 WX：nzkd01 
 */
/**
 *      This is NOT a freeware, use is subject to license terms
 *      应用名称: 邻客互动 应用中心
 *      应用开发者: 叶子
 *      开发者QQ: 987501448
 *      未经应用程序开发者/所有者的书面许可，不得进行反向工程、反向汇编、反向编译等，不得擅自复制、修改、链接、转载、汇编、发表、出版、发展与之有关的衍生产品、作品等
 *      版权所有: 互娱时代襄阳文化产业有限公司
 */
namespace app\appcenter\controller;

use app\appcenter\lib\AppInstallService;
use app\appcenter\lib\AppConfigService;
use lite\controller\Backend;
use lite\controller\traits\Crud;
use think\Request;
use think\facade\Cache;

class App extends Backend
{
    //------ Admin ------
    use Crud;

    public function detail(Request $request)
    {
        $params = $request->get();
        if (!empty($params['app']))
        {
            $data = AppConfigService::get($params['app'],'','',true);
            return success($data);
        }
        return error('获取失败');
    }
    public function update(Request $request)
    {
        if (!empty($request->post()))
        {
            foreach ($request->post() as $v){
                AppConfigService::set($v['app'],$v['key'],$v['value']);
            }
            return success('更新成功');
        }
        return error('更新失败');
    }

    public function myallapp()
    {
        $data = [];
        $dir_path = base_path()."plugin";
        if(file_exists($dir_path))
        {
            $file_list = array_diff(scandir($dir_path), array('.', '..'));
            
            foreach($file_list as $v)
            {
                $class = "app\\plugin\\".$v."\\\config"::class;
                if(class_exists($class)){
                    $data [$class::$app_key] = ['name'=>$class::$app_name, 'admin_lists'=>$class::$app_admin_lists];
                }

            }
        }
        return success('获取成功',$data);
    }

    public function installapp(Request $request)
    {
        
        $params = $request->post();
        if (!empty($params['id']))
        {
            $data = AppInstallService::install($params);
            return success('安装成功',$data);
        }

        return error('安装失败');
    }
    
    public function installverify(Request $request)
    {
           $key = $request->post('key');
        if (!empty($key) && Cache::has('install_'.$key))
        {
            cache('install_'.$key, NULL);
            return success('验证成功',md5($key.'151515aaa51%#@5v'));
        }
        return error('验证失败');
    }

    //插件 app端 控制器 勾子
    public function appmod(Request $request)
    {
        $app = $request->param('key');//APP标识
        $mod = $request->param('mod');//方法
        $class = "app\\plugin\\".$app."\\\controller\hook"::class;

        if (method_exists($class,$mod)) {        
            return (new $class())->$mod($request);
        }
        
    }


    //插件 API请求 勾子
    public function pluginhook(Request $request)
    {
        $app = $request->param('key');
        $mod = $request->param('mod');
        $class = "app\\plugin\\".$app."\\\hook\api\hook"::class;
        if (method_exists($class,$mod)) {        
            return (new $class())->$mod($request);
        }
        
    }

    

}
