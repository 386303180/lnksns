<?php
/*
 * 邻客互动 lnksns 高颜值论坛博客、社区圈子、积分商城系统 https://www.lnksns.vip
 * 邻客互动，为各类自媒体提供全面运营解决方案，助力自媒体运营成长。
 * 承接DQ、DX、DEDE、等各种PHP、SNS程序的定制开发业务,BUG修复业务！
 * 联系QQ：987501448 WX：nzkd01 
 */
/**
 *      This is NOT a freeware, use is subject to license terms
 *      应用名称: 邻客互动 应用中心
 *      应用开发者: 叶子
 *      开发者QQ: 987501448
 *      未经应用程序开发者/所有者的书面许可，不得进行反向工程、反向汇编、反向编译等，不得擅自复制、修改、链接、转载、汇编、发表、出版、发展与之有关的衍生产品、作品等
 *      版权所有: 互娱时代襄阳文化产业有限公司
 */
declare(strict_types=1);

namespace app\appcenter\lib;
use app\appcenter\model\AppConfigModel;

class AppConfigService
{
    /**
     * 设置配置
     */
    public static function set(string $app, string $key, $value)
    {
        $original = $value;
        if (is_array($value)) {
            $value = json_encode($value, JSON_UNESCAPED_UNICODE);
        }

        $data = AppConfigModel::where(['app' => $app, 'key' => $key])->findOrEmpty();

        if ($data->isEmpty()) {

        } else {
            $data->value = $value;
            $data->save();
            if (!is_null($value)) {
                $json = json_decode((string)$value, true);
                $value = json_last_error() === JSON_ERROR_NONE ? $json : $value;
            }
            cache('app_all_config_'.$app,null);
            cache('app_config_'.$app.'_'.$key, $value);
        }

        // 返回原始值
        return $original;
    }

    /**
     * 获取配置
     */
    public static function get(string $app, string $key = '', $default_value = null,$arr = false)
    {
        if (!empty($key)) {
            if(cache('app_config_'.$app.'_'.$key)){
                return cache('app_config_'.$app.'_'.$key);
            }
            $value = AppConfigModel::where(['app' => $app, 'key' => $key])->value('value');
            if (!is_null($value)) {
                $json = json_decode($value, true);
                $value = json_last_error() === JSON_ERROR_NONE ? $json : $value;
            }
            if ($value) {
                cache('app_config_'.$app.'_'.$key,$value);
                return $value;
            }
            // 返回特殊值 0 '0'
            if ($value === 0 || $value === '0') {
                cache('app_config_'.$app.'_'.$key,$value);
                return $value;
            }
            // 返回默认值
            if ($default_value !== null) {
                return $default_value;
            }
            // 返回本地配置文件中的值
            return config('project.' . $app . '.' . $key);
        }
        // 取某个类型下的所有name的值,返回数组 管理端用
        if($arr){
            $data = AppConfigModel::where(['app' => $app])->order('sort ASC')->select();
            foreach($data as &$value){
                if($value['type']==3 || $value['type']==2){
                    $value['value'] = (int) $value['value'];
                }elseif($value['type']==5){
                    if(!empty($value['config']))
                    {
                        $config_arr = explode(',',$value['config']);
                        $new_config = [];
                        foreach($config_arr as $v){
                            $config_value = explode(':',$v);
                            $new_config[] = ['value'=>$config_value[0],'name'=>$config_value[1]];
                        }
                        $value['config'] = $new_config;
                    }
                }
            }

            return $data;
        }        

        // 取某个类型下的所有name的值 APP端用
        if(cache('app_all_config_'.$app)){
            return cache('app_all_config_'.$app);
        }
        $data = AppConfigModel::where(['app' => $app])->column('type,value', 'key');
        foreach ($data as $k => $v) {
            if($v['type']==2 || $v['type']==3){
                $data[$k] = (int)$v['value'];
            }else{
                $json = json_decode($v['value'], true);
                if (json_last_error() === JSON_ERROR_NONE) {
                    $data[$k] = $json;
                }else{
                    $data[$k] = $v['value'];
                }
            }
        }
        if ($data) {
            cache('app_all_config_'.$app,$data);
            return $data;
        }
    }



}
