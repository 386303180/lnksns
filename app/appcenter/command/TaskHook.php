<?php
declare (strict_types = 1);

namespace app\appcenter\command;

use think\console\Command;
use think\console\Input;
use think\console\input\Argument;
use think\console\input\Option;
use think\console\Output;

class TaskHook extends Command
{
    protected function configure()
    {
        // 指令配置
        $this->setName('taskhook')
        ->addArgument('app', Argument::OPTIONAL, "your app")
        ->addArgument('method', Argument::OPTIONAL, "your method")
        ->setDescription('taskhook Command');
    }
    protected function execute(Input $input, Output $output)
    {
        // 指令输出
        $app = trim($input->getArgument('app'));
        $method = trim($input->getArgument('method'));
        if($app && $method){

            $class = "app\\plugin\\".$app."\\command\\hook".''::class;
            if (method_exists($class,$method)) {
                $class::$method();
            }
        }

        $output->writeln("Success");
    }
}
