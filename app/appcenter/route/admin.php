<?php
/*
 * 邻客互动 lnksns 高颜值论坛博客、社区圈子、积分商城系统 https://www.lnksns.vip
 * 邻客互动，为各类自媒体提供全面运营解决方案，助力自媒体运营成长。
 * 承接DQ、DX、DEDE、等各种PHP、SNS程序的定制开发业务,BUG修复业务！
 * 联系QQ：987501448 WX：nzkd01 
 */
/**
 *      This is NOT a freeware, use is subject to license terms
 *      应用名称: 邻客互动 应用中心
 *      应用开发者: 叶子
 *      开发者QQ: 987501448
 *      未经应用程序开发者/所有者的书面许可，不得进行反向工程、反向汇编、反向编译等，不得擅自复制、修改、链接、转载、汇编、发表、出版、发展与之有关的衍生产品、作品等
 *      版权所有: 互娱时代襄阳文化产业有限公司
 */
use think\facade\Route;

Route::group('/',function(){

    // 获取系统配置
    Route::get('/config/detail','App/detail');
    // 更新系统配置
    Route::post('/config/update','App/update');

    Route::get('/myallapp','App/myallapp');
    Route::post('/installapp','App/installapp');

    
    Route::get('/myapplists','MyAppLists/index');
    Route::post('/myapplistspost/create','MyAppLists/create');
    Route::post('/myapplistspost/update','MyAppLists/update');
    Route::get('/myapplistspost/detail','MyAppLists/detail');
    Route::delete('/myapplistspost/delete','MyAppLists/delete');
    
    
    
})->middleware('check_login','admin');




