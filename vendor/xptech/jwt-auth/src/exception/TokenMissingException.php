<?php

namespace xptech\jwt\exception;

class TokenMissingException extends JWTException
{
    protected $message = 'token missing';
}
