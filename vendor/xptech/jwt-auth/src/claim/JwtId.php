<?php


namespace xptech\jwt\claim;

class JwtId extends Claim
{
    protected $name = 'jti';
}
