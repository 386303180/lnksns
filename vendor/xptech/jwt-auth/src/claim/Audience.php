<?php

namespace xptech\jwt\claim;

class Audience extends Claim
{
    protected $name = 'aud';
}
