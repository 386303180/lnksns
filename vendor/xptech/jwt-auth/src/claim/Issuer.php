<?php


namespace xptech\jwt\claim;

class Issuer extends Claim
{
    protected $name = 'iss';
}
