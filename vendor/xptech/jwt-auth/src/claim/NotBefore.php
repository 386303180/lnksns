<?php


namespace xptech\jwt\claim;

class NotBefore extends Claim
{
    protected $name = 'nbf';
}
