<?php


namespace xptech\jwt;

use xptech\jwt\command\SecretCommand;
use xptech\jwt\middleware\InjectJwt;
use xptech\jwt\provider\JWT as JWTProvider;

class Service extends \think\Service
{
    public function boot()
    {
        $this->commands(SecretCommand::class);

        (new JWTProvider())->register();
    }
}
